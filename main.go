// Copyright 2024 The ngrab Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Command ngrab ingests Tcl/Tk .n files and packs them into a JSON document.
//
// # Installation
//
//	$ go install modernc.org/ngrab
//
// # Usage
//
//	ngrab path
//
// If path is a regular file then that single file is processed.
//
// If path is a directory then all .n files under root will be processed.
//
// In both cases the result is written to stdout as a single JSON document.
package main // import "modernc.org/ngrab"

import (
	"bufio"
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"modernc.org/ngrab/lib"
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		fail(2, "expected 1 argument")
	}

	arg := flag.Arg(0)
	fi, err := os.Stat(arg)
	if err != nil {
		fail(1, "%v", err)
	}

	var files []string
	switch {
	case fi.Mode().IsRegular():
		files = append(files, arg)
	case fi.IsDir():
		err := filepath.Walk(arg, func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if info.IsDir() || !info.Mode().IsRegular() || !strings.HasSuffix(path, ".n") {
				return nil
			}

			files = append(files, path)
			return nil
		})
		if err != nil {
			fail(1, "%v", err)
		}
	}

	w := bufio.NewWriter(os.Stdout)

	defer w.Flush()

	t, err := lib.NewTask(w, files)
	if err != nil {
		fail(1, "%v", err)
	}

	if err = t.Main(); err != nil {
		fail(1, "%v", err)
	}
}
