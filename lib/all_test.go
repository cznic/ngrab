// Copyright 2024 The ngrap Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package lib // import "modernc.org/ngrab/lib"

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"
)

var (
	oOut = flag.String("w", "", "")
)

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

func Test(t *testing.T) {
	tcl, err := filepath.Glob(filepath.Join("testdata/tcl9.0/*.n"))
	if err != nil {
		t.Fatal(err)
	}

	tk, err := filepath.Glob(filepath.Join("testdata/tk9.0/*.n"))
	if err != nil {
		t.Fatal(err)
	}

	t.Run("tcl", func(t *testing.T) { test(t, "tcl", tcl) })
	t.Run("tk", func(t *testing.T) { test(t, "tk", tk) })
}

func test(t *testing.T, tag string, files []string) {
	w := io.Discard
	if *oOut != "" {
		f, err := os.Create(fmt.Sprintf("%s%s.json", *oOut, tag))
		if err != nil {
			t.Fatal(err)
		}

		trc("", f.Name())
		defer f.Close()

		bw := bufio.NewWriter(f)

		defer bw.Flush()

		w = bw
	}
	task, err := NewTask(w, files)
	if err != nil {
		t.Fatal(err)
	}

	if err := task.Main(); err != nil {
		t.Fatal(err)
	}
}
